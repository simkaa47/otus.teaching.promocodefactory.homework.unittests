﻿using Microsoft.Extensions.DependencyInjection;
using Otus.PromoCodeFactory.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerDatabaseFixture : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        public IServiceCollection ServiceCollection { get; set; }


        public PartnerDatabaseFixture()
        {
            ServiceCollection = Configuration.GetServiceCollection();
            ServiceProvider = GetServiceProvider();
        }

        IServiceProvider GetServiceProvider()
        {
            return ServiceCollection.ConfigureInMemoryContext()
                .BuildServiceProvider();
        }

        public void Dispose()
        {
            
        }
    }
}
