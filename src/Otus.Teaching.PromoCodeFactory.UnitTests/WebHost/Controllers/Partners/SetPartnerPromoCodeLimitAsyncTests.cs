﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests: IClassFixture<PartnerDatabaseFixture>
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private Partner _partner;
        private Guid _id;
        private SetPartnerPromoCodeLimitRequest _request;
        private IRepository<Partner> _fakePartnerRepository;

        public SetPartnerPromoCodeLimitAsyncTests(PartnerDatabaseFixture databaseFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _partner = PartnerBuilder();
            _id = Guid.NewGuid();
            _request = SetPartnerLimitRequestBulder();
            collection = ServiceBuilder.GetServiceCollection();
            _fakePartnerRepository = databaseFixture.ServiceProvider.GetService<IRepository<Partner>>();
        }

        IServiceCollection collection;


        /// <summary>
        /// Если партнер не найден, нужно вернуть 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnNotFound()
        {
            // Arrange
            _partner = null;
            SetRepoBehavuior();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, нужно вернуть 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotActive_ReturnBadRequest()
        {
            // Arrange         
            
            _partner.IsActive = false;
            SetRepoBehavuior();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            
        }

        /// <summary>
        /// Если лимит закончился, то нужно вернуть 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitFinished_ReturnBadRequest()
        {
            // Arrange
            _request.EndDate = DateTime.Today.AddDays(-1);
            SetRepoBehavuior();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если лимит действующий, обнулить список выданных промокодов
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsOk_ClearNumPromocodes()
        {
            // Arrange                      
            _partner.NumberIssuedPromoCodes = 10;
            SetRepoBehavuior();

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            _partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsNegative_ReturnBadRequest()
        {
            // Arrange
            _request.Limit = -6;           
            SetRepoBehavuior();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(_id, _request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        /// <summary>
        /// Проверка успешного сохранения  в БД
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsOk_SaveIsSuccess()
        {
            // Arrange
            var newPartnerConroller = PartnersControllerBuilder();
            await _fakePartnerRepository.AddAsync(_partner);
            var oldCountLimit = _partner.PartnerLimits.Count;

            //Act
            await newPartnerConroller.SetPartnerPromoCodeLimitAsync(_partner.Id, _request);
            var updatedPartner = await _fakePartnerRepository.GetByIdAsync(_partner.Id);

            //Assert

            //Количество лимитов должно прибавиться на 1
            updatedPartner.PartnerLimits.Count.Should().Be(oldCountLimit + 1);
        }


        SetPartnerPromoCodeLimitRequest SetPartnerLimitRequestBulder()
        {
            return new Fixture().Build<SetPartnerPromoCodeLimitRequest>()
                .With(r=>r.EndDate ,DateTime.Now.AddDays(100)) // Ставим условие незакончившегося лимтта
                .Create();
        }

        Partner PartnerBuilder()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Настройка поведения мока репозитория
        /// </summary>
        void SetRepoBehavuior()
        {
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_id))
                    .ReturnsAsync(_partner);
        }

        PartnersController PartnersControllerBuilder()
        {
            return new PartnersController(_fakePartnerRepository);
        }

    }


}