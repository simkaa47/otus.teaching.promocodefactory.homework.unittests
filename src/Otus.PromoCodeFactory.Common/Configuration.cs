﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.PromoCodeFactory.Common
{
    public static class Configuration
    {
        public static IServiceCollection GetServiceCollection(IServiceCollection services = null)
        {
            return ServiceBuilder.GetServiceCollection();
        }

        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("PromoCodeFactoryDbFake", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });            
            return services;
        }
    }
}
